# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Katiane Cherlen Martins| [@katy_martins](https://gitlab.com/katy_martins)| 

# Documentação

A documentação do projeto pode ser acessada pelo link:

>  https://kattycherlen.wixsite.com/musicacomarduino

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)
* [Wix site](https://wix.com)
